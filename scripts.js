$().ready(function(){
	$("#formularioInscricao").validate({

		rules: {
			nome: {
				required: true,
				minlength: 4
			},
			endereco: {
				required: true,
				minlength: 10
			},
			email: {
				required: true,
				email: true
			},
			agree: "required"
		},

		messages: {
			nome:{
				required: "Por favor digite um nome",
				minlength: "O nome deve possuir mais de 3 caracteres"
			},
			endereco: {
				required: "Por favor digite um endereco",
				minlength: "O endereco deve possuir mais de 10 caracteres"
			},
			email: {
				required: "Por favor digite um email",
				email: "Por favor insira um email válido"
			},
			agree: "Voce tem de aceitar os termos de responsabilidade."
		}

	});
});